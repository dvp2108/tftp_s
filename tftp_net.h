#ifndef TFTP_NET_H
#define TFTP_NET_H

#include <stdint.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>

#define RRQ 1
#define WRQ 2
#define DAT 3
#define ACK 4
#define ERR 5
#define MODE_ASCII  netascii
#define MODE_OCTET  octet
#define ERR_UNKNOWN     1
#define ERR_NOTFOUND    2
#define ERR_ACCESSDENIER    3
#define ERR_DISKFULL    4
#define ERR_CLOBBER     5
#define ERR_UNKNOWNUSER 6


int openudp (uint16_t, struct in_addr*);
void *myrecv (int, size_t*, struct sockaddr_in *, int);
void derrno (int, struct sockaddr_in*);
void mysend (int, char *, size_t, struct sockaddr_in *);
void sendack (int , uint16_t , struct sockaddr_in *);
void senderr (int, uint16_t, struct sockaddr_in *);

#endif
